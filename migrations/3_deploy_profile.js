var SourceCheckProfile = artifacts.require("./SourceCheckProfile.sol");

module.exports = function(deployer, network, accounts) {
  const profileOwner = accounts[1];
  deployer.deploy(SourceCheckProfile, profileOwner);
};
