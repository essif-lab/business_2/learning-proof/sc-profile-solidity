// SPDX-License-Identifier: MIT
pragma solidity >=0.4.22 <0.9.0;
import "@openzeppelin/contracts/token/ERC20/ERC20.sol";
import "@openzeppelin/contracts/utils/math/SafeMath.sol";

contract SourceCheckProfile {
  
  // state variables
  address public _owner;
  address public _serviceAddr;
  uint16 public _serviceFeePerc; // 0000 to 9999
  string public _profileUrl;

  // member address => (ERC20 token address => withdraw amount)
  mapping (address => mapping(address => uint256)) private _withdraws;

  // member address => native token withdraw amount
  mapping (address => uint256) private _nativeWithdraws;

  // events triggered on withdraw
  event EvtNativeWithdraw();
  event EvtWithdraw(address indexed tokenAddr);
    
  constructor(
    address owner_, 
    address serviceAddr_, 
    uint16 serviceFeePerc_,
    string memory profileUrl_
  ) {
    _owner = owner_;
    _serviceAddr = serviceAddr_;
    _serviceFeePerc = serviceFeePerc_;
    _profileUrl = append(profileUrl_, toAsciiString(address(this)));
  }
  
  receive() external payable {}

  function append(string memory a, string memory b) internal pure returns (string memory) {
    return string(abi.encodePacked(a, b));
  } 
  
  function toAsciiString(address x) internal pure returns (string memory) {
    bytes memory s = new bytes(40);
    for (uint i = 0; i < 20; i++) {
        bytes1 b = bytes1(uint8(uint(uint160(x)) / (2**(8*(19 - i)))));
        bytes1 hi = bytes1(uint8(b) / 16);
        bytes1 lo = bytes1(uint8(b) - 16 * uint8(hi));
        s[2*i] = char(hi);
        s[2*i+1] = char(lo);            
    }
    return string(s);
  }

  function char(bytes1 b) internal pure returns (bytes1 c) {
      if (uint8(b) < 10) return bytes1(uint8(b) + 0x30);
      else return bytes1(uint8(b) + 0x57);
  }
  
  function maxNativeWithdraw() public view 
    returns (uint256) 
  {
    require(msg.sender == _owner, 'Only the owner of the profile can check available balance!');
    uint256 nativeBalance = address(this).balance;
    uint256 memberNativeWithdrawn = _nativeWithdraws[msg.sender];
    uint256 serviceNativeWithdrawn = _nativeWithdraws[_serviceAddr];
    uint256 totalDeposited = nativeBalance + memberNativeWithdrawn + serviceNativeWithdrawn;
    uint256 memberWithdraw = ((totalDeposited / 10000) * (10000 - _serviceFeePerc)) - memberNativeWithdrawn;
    return memberWithdraw;
  }

  function maxWithdraw(address tokenAddr) public view 
    returns (uint256) 
  {
    require(msg.sender == _owner, 'Only the owner of the profile can check available balance!');
    uint256 tokenBalance = ERC20(tokenAddr).balanceOf(address(this));
    uint256 memberTotalWithdrawn = _withdraws[msg.sender][tokenAddr];
    uint256 serviceTotalWithdrawn = _withdraws[_serviceAddr][tokenAddr];
    uint256 totalDeposited = tokenBalance + memberTotalWithdrawn + serviceTotalWithdrawn;
    uint256 memberWithdraw = ((totalDeposited / 10000) * (10000 - _serviceFeePerc)) - memberTotalWithdrawn;
    return memberWithdraw;
  }

  function nativeWithdraw() external {
    require(msg.sender == _owner, 'Only the owner of the profile can withdraw tokens!');
    
    // Get current native token balance and withdrawn amounts
    uint256 nativeBalance = address(this).balance;
    uint256 memberTotalWithdrawn = _nativeWithdraws[msg.sender];
    uint256 serviceTotalWithdrawn = _nativeWithdraws[_serviceAddr];
    
    // Calculate available withdraw amounts
    uint256 totalDeposited = nativeBalance + memberTotalWithdrawn + serviceTotalWithdrawn;
    uint256 memberWithdraw = ((totalDeposited / 10000) * (10000 - _serviceFeePerc)) - memberTotalWithdrawn;
    uint256 serviceWithdraw = nativeBalance - memberWithdraw;

    // Update withdrawn amounts
    _nativeWithdraws[msg.sender] = _nativeWithdraws[msg.sender] + memberWithdraw;
    _nativeWithdraws[_serviceAddr] = _nativeWithdraws[_serviceAddr] + serviceWithdraw;
    
    // Transfer tokens
    (bool sent, ) = msg.sender.call{value: memberWithdraw}("");
    require(sent, "Failed to send Ether");
    
    (bool sentToService, ) = _serviceAddr.call{value: serviceWithdraw}("");
    require(sentToService, "Failed to send Ether");

    // Emit event
    emit EvtNativeWithdraw();
  }

  function withdraw(address tokenAddr) external {
    require(msg.sender == _owner, 'Only the owner of the profile can withdraw tokens!');
    
    // Get current token balance and withdrawn amounts
    uint256 tokenBalance = ERC20(tokenAddr).balanceOf(address(this));
    uint256 memberTotalWithdrawn = _withdraws[msg.sender][tokenAddr];
    uint256 serviceTotalWithdrawn = _withdraws[_serviceAddr][tokenAddr];
    
    // Calculate available withdraw amounts
    uint256 totalDeposited = tokenBalance + memberTotalWithdrawn + serviceTotalWithdrawn;
    uint256 memberWithdraw = ((totalDeposited / 10000) * (10000 - _serviceFeePerc)) - memberTotalWithdrawn;
    uint256 serviceWithdraw = tokenBalance - memberWithdraw;

    // Update withdrawn amounts
    _withdraws[msg.sender][tokenAddr] = _withdraws[msg.sender][tokenAddr] + memberWithdraw;
    _withdraws[_serviceAddr][tokenAddr] = _withdraws[_serviceAddr][tokenAddr] + serviceWithdraw;
    
    // Transfer tokens
    ERC20(tokenAddr).transfer(msg.sender, memberWithdraw);
    ERC20(tokenAddr).transfer(_serviceAddr, serviceWithdraw);

    // Emit event
    emit EvtWithdraw(tokenAddr);
  }
}