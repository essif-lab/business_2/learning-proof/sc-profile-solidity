// SPDX-License-Identifier: MIT
pragma solidity >=0.4.22 <0.9.0;

import "./SourceCheckProfile.sol";

contract SourceCheckProfileFactory {
  address[] _deployedContracts;
  
  event ProfileCreated(address indexed owner, address indexed profileAddr);

  function createNew(address owner, address serviceAddr, uint16 serviceFeePerc, string memory profileUrl) public {
    SourceCheckProfile scp = new SourceCheckProfile(owner, serviceAddr, serviceFeePerc, profileUrl);
    _deployedContracts.push(address(scp));
    emit ProfileCreated(owner, address(scp));
  }
}