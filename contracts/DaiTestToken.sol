// SPDX-License-Identifier: MIT
pragma solidity >=0.4.22 <0.9.0;

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";

contract DaiTestToken is ERC20 {
  address public admin;
  
  constructor() ERC20('Dai Stablecoin', 'DAI') {
    admin = msg.sender;
    _mint(msg.sender, 10000 * (10**18));
  }

  function mint(address to, uint amount) external {
    require(msg.sender == admin, 'only admin');
    _mint(to, amount);
  }

  function burn(uint amount) external {
    _burn(msg.sender, amount);
  }
  
}
